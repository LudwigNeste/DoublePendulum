function to2Hex(c) {
    if (c < 0 || c > 1)
        return "00";
    var hex = Math.floor((c * 255)).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}
function rgbaToHex(r, g, b, a) {
    return "#" + to2Hex(r) + to2Hex(g) + to2Hex(b) + to2Hex(a);
}
class SliderInput {
    constructor(input, printValue, min, max, step, defaultValue, precision = 2) {
        this.input = input;
        this.printValue = printValue;
        input.min = String(min);
        input.max = String(max);
        input.step = String(step);
        input.value = String(defaultValue);
        this.printValue.innerHTML = defaultValue.toFixed(precision);
        input.addEventListener("change", () => {
            this.printValue.innerHTML = this.valueAsNumber.toFixed(precision);
        });
    }
    get valueAsNumber() {
        return this.input.valueAsNumber;
    }
    addChangeListener(listener) {
        this.input.addEventListener("change", listener);
    }
}
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
class PendulumDrawer {
    constructor(canvas, settings) {
        this.aa1 = 0.0;
        this.aa2 = 0.0;
        this.av1 = 1.0;
        this.av2 = 0.0;
        this.a1 = 0.0;
        this.a2 = 0.0;
        this.l1 = 1.0;
        this.l2 = 1.0;
        this.m1 = 1.0;
        this.m2 = 1.0;
        this.g = 9.81;
        this.running = false;
        this.scale = 1;
        this.phyTime = 1;
        this.timeScale = 1;
        this.mouseDown = false;
        this.mouseX = 0;
        this.mouseY = 0;
        this.numTracePoints = 100;
        this.tracePoints = [];
        this.canvas = canvas;
        this.settings = settings;
        this.context = canvas.getContext("2d");
        window.addEventListener("resize", (event) => { handler.setWindowSizes(); }, false);
        let inputs = settings.getElementsByTagName("input");
        let values = settings.getElementsByClassName("value");
        let inputl1 = new SliderInput(inputs[0], values[0], 0.1, 10, 0.1, 1);
        inputl1.addChangeListener(() => {
            this.l1 = inputl1.valueAsNumber;
            this.onValueChange();
        });
        let inputl2 = new SliderInput(inputs[1], values[1], 0.1, 10, 0.1, 1);
        inputl2.addChangeListener(() => {
            this.l2 = inputl2.valueAsNumber;
            this.onValueChange();
        });
        let inputm1 = new SliderInput(inputs[2], values[2], 0.1, 20, 0.1, 1);
        inputm1.addChangeListener(() => {
            this.m1 = inputm1.valueAsNumber;
            this.onValueChange();
        });
        let inputm2 = new SliderInput(inputs[3], values[3], 0.1, 20, 0.1, 1);
        inputm2.addChangeListener(() => {
            this.m2 = inputm2.valueAsNumber;
            this.onValueChange();
        });
        let inputg = new SliderInput(inputs[4], values[4], 0, 100, 0.1, 9.81);
        inputg.addChangeListener(() => {
            this.g = inputg.valueAsNumber;
            this.onValueChange();
        });
        let inputT = new SliderInput(inputs[5], values[5], 0.001, 5, 0.001, 1, 3);
        inputT.addChangeListener(() => {
            this.phyTime = inputT.valueAsNumber;
            this.onValueChange();
        });
        let inputS = new SliderInput(inputs[6], values[6], 0.01, 2, 0.1, 1);
        inputS.addChangeListener(() => {
            this.timeScale = inputS.valueAsNumber;
            this.onValueChange();
        });
        let inputTrace = new SliderInput(inputs[7], values[7], 0, 10000, 10, 100, 0);
        inputTrace.addChangeListener(() => {
            console.log(inputTrace.valueAsNumber);
            this.numTracePoints = inputTrace.valueAsNumber;
            this.onValueChange();
        });
        this.energyText = values[8];
        canvas.addEventListener("mousedown", () => { this.mouseDown = true; });
        canvas.addEventListener("mouseup", () => { this.mouseDown = false; });
        canvas.addEventListener("mousemove", (event) => {
            this.mouseX = event.offsetX - this.xc;
            this.mouseY = event.offsetY - this.yc;
        });
        canvas.addEventListener("touchstart", (event) => {
            this.mouseDown = true;
            this.mouseX = event.touches[0].clientX - this.canvas.getBoundingClientRect().left - this.xc;
            this.mouseY = event.touches[0].clientY - this.canvas.getBoundingClientRect().top - this.yc;
        });
        canvas.addEventListener("touchend", () => { this.mouseDown = false; });
        canvas.addEventListener("touchmove", (event) => {
            this.mouseX = event.touches[0].clientX - this.canvas.getBoundingClientRect().left - this.xc;
            this.mouseY = event.touches[0].clientY - this.canvas.getBoundingClientRect().top - this.yc;
        });
        this.toggleButton = settings.getElementsByClassName("abort")[0];
        this.toggleButton.onclick = () => this.toggle();
        this.context.font = "15px Arial";
        let text = this.context.measureText("1m");
        this.textWidth = text.width;
        this.setWindowSizes();
    }
    toggle() {
        if (this.running) {
            this.toggleButton.innerHTML = "Start!";
            this.running = false;
        }
        else {
            this.toggleButton.innerHTML = "Stop!";
            this.running = true;
        }
    }
    start() {
        this.lastTime = Date.now();
        this.running = true;
        this.interval = setInterval(() => this.redraw(), 30);
    }
    onValueChange() {
        if (this.canvas.width > this.canvas.height) {
            this.scale = this.yc / (this.l1 + this.l2);
        }
        else {
            this.scale = this.xc / (this.l1 + this.l2);
        }
        this.scale *= 0.95;
    }
    setWindowSizes() {
        let width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
        let height = window.innerHeight
            || document.body.clientHeight
            || document.documentElement.clientHeight;
        this.canvas.height = height * 0.9;
        if (width > height)
            this.canvas.width = width * 0.9 - settings.offsetWidth;
        else
            this.canvas.width = width * 0.9;
        this.xc = this.canvas.width / 2.0;
        this.yc = this.canvas.height / 2.0;
        this.onValueChange();
        this.redraw();
    }
    calcPhysicalFrame(dT) {
        let denom = 2 * this.m1 + this.m2 - this.m2 * Math.cos(2 * this.a1 - 2 * this.a2);
        this.aa1 = (-this.g * (2 * this.m1 + this.m2) * Math.sin(this.a1) - this.m2 * this.g * Math.sin(this.a1 - 2 * this.a2) - 2 * Math.sin(this.a1 - this.a2) * this.m2 * (this.av2 * this.av2 * this.l2 + this.av1 * this.av1 * this.l1 * Math.cos(this.a1 - this.a2)))
            / (this.l1 * denom);
        this.aa2 = (2 * Math.sin(this.a1 - this.a2) * (this.av1 * this.av1 * this.l1 * (this.m1 + this.m2) + this.g * (this.m1 + this.m2) * Math.cos(this.a1) + this.av2 * this.av2 * this.l1 * this.m2 * Math.cos(this.a1 - this.a2)))
            / (this.l2 * denom);
        this.av1 += this.aa1 * dT / 1000;
        this.av2 += this.aa2 * dT / 1000;
        this.a1 += this.av1 * dT / 1000;
        this.a2 += this.av2 * dT / 1000;
    }
    redraw() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        if (this.mouseDown) {
            this.aa1 = 0.0;
            this.av1 = 0.0;
            this.aa2 = 0.0;
            this.av2 = 0.0;
            if (this.mouseX * this.mouseX + this.mouseY * this.mouseY < this.l1 * this.l1 * this.scale * this.scale) {
                if (Math.abs(this.mouseY) < 1)
                    this.a1 = Math.PI / 2.0 * Math.sign(this.mouseX);
                else if (this.mouseY < 0)
                    this.a1 = Math.atan(this.mouseX / this.mouseY) + Math.PI;
                else
                    this.a1 = Math.atan(this.mouseX / this.mouseY);
            }
            else {
                if (Math.abs(this.mouseY) < 1)
                    this.a1 = Math.PI / 2.0 * Math.sign(this.mouseX);
                else if (this.mouseY < 0)
                    this.a2 = Math.atan(this.mouseX / this.mouseY) + Math.PI;
                else
                    this.a2 = Math.atan(this.mouseX / this.mouseY);
            }
        }
        else if (this.running) {
            let dT = Date.now() - this.lastTime;
            while (dT > this.phyTime) {
                this.calcPhysicalFrame(this.timeScale * this.phyTime);
                dT -= this.phyTime;
            }
            if (dT > 0) {
                this.calcPhysicalFrame(this.timeScale * dT);
            }
        }
        this.x1 = Math.floor(Math.sin(this.a1) * this.l1 * this.scale);
        this.y1 = Math.floor(Math.cos(this.a1) * this.l1 * this.scale);
        this.x2 = this.x1 + Math.floor(Math.sin(this.a2) * this.l2 * this.scale);
        this.y2 = this.y1 + Math.floor(Math.cos(this.a2) * this.l2 * this.scale);
        if (!(this.mouseDown || !this.running))
            this.tracePoints.push(new Point(this.x2 + this.xc, this.y2 + this.yc));
        while (this.tracePoints.length > this.numTracePoints)
            this.tracePoints.shift();
        this.context.beginPath();
        this.context.lineWidth = 1;
        this.context.strokeStyle = "#FFFFFF";
        for (let p of this.tracePoints) {
            this.context.lineTo(p.x, p.y);
            this.context.moveTo(p.x, p.y);
        }
        this.context.stroke();
        this.context.beginPath();
        this.context.fillStyle = "#FFFFFF";
        this.context.strokeStyle = "#FFFFFF";
        this.context.lineWidth = 1;
        this.context.font = "15px Arial";
        this.context.fillText("1m", 10 + (this.scale - this.textWidth + 10) / 2.0, this.canvas.height - 16);
        this.context.moveTo(20, this.canvas.height - 15);
        this.context.lineTo(20, this.canvas.height - 5);
        this.context.moveTo(20, this.canvas.height - 10);
        this.context.lineTo(10 + this.scale, this.canvas.height - 10);
        this.context.moveTo(10 + this.scale, this.canvas.height - 15);
        this.context.lineTo(10 + this.scale, this.canvas.height - 5);
        this.context.stroke();
        this.context.lineWidth = 2;
        this.context.beginPath();
        this.context.strokeStyle = "#0000FF";
        this.context.moveTo(this.xc + this.x1, this.yc + this.y1);
        this.context.lineTo(this.xc + this.x2, this.yc + this.y2);
        this.context.stroke();
        this.context.beginPath();
        this.context.fillStyle = "#0000FF";
        this.context.arc(this.xc + this.x2, this.yc + this.y2, 5 * Math.sqrt(this.m2), 0.0, 2.0 * Math.PI);
        this.context.fill();
        this.context.beginPath();
        this.context.strokeStyle = "#FF0000";
        this.context.moveTo(this.xc, this.yc);
        this.context.lineTo(this.xc + this.x1, this.yc + this.y1);
        this.context.stroke();
        this.context.beginPath();
        this.context.fillStyle = "#FF0000";
        this.context.arc(this.xc + this.x1, this.yc + this.y1, 5 * Math.sqrt(this.m1), 0.0, 2.0 * Math.PI);
        this.context.fill();
        this.energyText.innerHTML = (((this.m1 + this.m2) / 2.0) * this.l1 * this.l1 * this.av1 * this.av1 + (this.m2 / 2.0) * this.l2 * this.l2 * this.av2 * this.av2
            + this.m2 * this.l1 * this.l2 * this.av1 * this.av2 * Math.cos(this.a1 - this.a2) - (this.m1 + this.m2) * this.g * this.l1 * Math.cos(this.a1)
            - this.m2 * this.g * this.l2 * Math.cos(this.a2) + (this.m1 + this.m2) * this.g * this.l1
            + this.m2 * this.g * this.l2).toFixed(3);
        this.lastTime = Date.now();
    }
}
let canvas = document.getElementById("myCanvas");
let settings = document.getElementById("settings");
let handler = new PendulumDrawer(canvas, settings);
handler.start();
