function to2Hex(c: number): string
{
    if (c < 0 || c > 1)
        return "00";
    var hex = Math.floor((c * 255)).toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbaToHex(r: number, g: number, b: number, a: number): string
{
    return "#" + to2Hex(r) + to2Hex(g) + to2Hex(b) + to2Hex(a);
}


class SliderInput
{
    input: HTMLInputElement;
    printValue: Element;

    private _valueAsNumber: number;
    constructor(input: HTMLInputElement, printValue: Element, min: number, max: number, step: number, defaultValue: number, precision: number = 2)
    {
        this.input = input;
        this.printValue = printValue;

        input.min = String(min);
        input.max = String(max);
        input.step = String(step);
        input.value = String(defaultValue);

        this.printValue.innerHTML = defaultValue.toFixed(precision);

        input.addEventListener("change", () =>
        {
            this.printValue.innerHTML = this.valueAsNumber.toFixed(precision);
        });
    }

    get valueAsNumber(): number 
    {
        return this.input.valueAsNumber;
    }

    public addChangeListener(listener)
    {
        this.input.addEventListener("change", listener);
    }
}

class Point
{
    public x: number;
    public y: number;

    public constructor(x: number, y: number)
    {
        this.x = x;
        this.y = y;
    }
}

/**
 * Wrapper class for drawing the pendulum
 */
class PendulumDrawer
{
    //HTML elements 
    private settings: HTMLElement;
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    // angular values
    private aa1: number = 0.0; aa2: number = 0.0;
    private av1: number = 1.0; av2: number = 0.0;
    private a1: number = 0.0; a2: number = 0.0;

    // physical setup constants
    private l1: number = 1.0; l2: number = 1.0;
    private m1: number = 1.0; m2: number = 1.0;
    private g: number = 9.81;

    //last time calculation finished
    private lastTime: number;

    //Id of the interval to close it again
    private interval: number;
    private toggleButton: HTMLButtonElement;
    private running: boolean = false;

    //coordinates
    //center coordinates to not recalculate that every time
    private xc: number; private yc: number;
    //mass points
    private x1: number; private y1: number;
    private x2: number; private y2: number;
    //scale
    private scale: number = 1;

    private phyTime: number = 1;
    private timeScale: number = 1;

    private mouseDown: boolean = false;
    private mouseX: number = 0; private mouseY: number = 0;

    //Text widht
    private textWidth: number; private textHeight: number;

    //energy
    private energyText: Element;

    //point tracing
    private tracePoints: Point[];
    private numTracePoints: number = 100;

    /**
     * Initializes the class
     * @param canvas the canvas on which the drawing will take place 
     * @param settings the settings which are parsed in
     */
    public constructor(canvas: HTMLCanvasElement, settings: HTMLElement)
    {
        this.tracePoints = [];
        this.canvas = canvas;
        this.settings = settings;
        this.context = canvas.getContext("2d");

        // Add resize listener
        window.addEventListener("resize", (event) => { handler.setWindowSizes(); }, false);

        // Bind value change callback
        let inputs: HTMLCollectionOf<HTMLInputElement> = settings.getElementsByTagName("input");
        let values: HTMLCollectionOf<Element> = settings.getElementsByClassName("value");

        let inputl1 = new SliderInput(inputs[0], values[0], 0.1, 10, 0.1, 1);
        inputl1.addChangeListener(() =>
        {
            this.l1 = inputl1.valueAsNumber; this.onValueChange();
        });
        let inputl2 = new SliderInput(inputs[1], values[1], 0.1, 10, 0.1, 1);
        inputl2.addChangeListener(() =>
        {
            this.l2 = inputl2.valueAsNumber; this.onValueChange();
        });
        let inputm1 = new SliderInput(inputs[2], values[2], 0.1, 20, 0.1, 1);
        inputm1.addChangeListener(() =>
        {
            this.m1 = inputm1.valueAsNumber; this.onValueChange();
        });
        let inputm2 = new SliderInput(inputs[3], values[3], 0.1, 20, 0.1, 1);
        inputm2.addChangeListener(() =>
        {
            this.m2 = inputm2.valueAsNumber; this.onValueChange();
        });
        let inputg = new SliderInput(inputs[4], values[4], 0, 100, 0.1, 9.81);
        inputg.addChangeListener(() =>
        {
            this.g = inputg.valueAsNumber; this.onValueChange();
        });
        let inputT = new SliderInput(inputs[5], values[5], 0.001, 5, 0.001, 1, 3);
        inputT.addChangeListener(() =>
        {
            this.phyTime = inputT.valueAsNumber; this.onValueChange();
        });
        let inputS = new SliderInput(inputs[6], values[6], 0.01, 2, 0.1, 1);
        inputS.addChangeListener(() =>
        {
            this.timeScale = inputS.valueAsNumber; this.onValueChange();
        });
        let inputTrace = new SliderInput(inputs[7], values[7], 0, 10000, 10, 100, 0);
        inputTrace.addChangeListener(() =>
        {
            console.log(inputTrace.valueAsNumber)
            this.numTracePoints = inputTrace.valueAsNumber; this.onValueChange();
        });

        this.energyText = values[8];

        // Mouse input bindings
        canvas.addEventListener("mousedown", () => { this.mouseDown = true; });
        canvas.addEventListener("mouseup", () => { this.mouseDown = false; });
        canvas.addEventListener("mousemove", (event: MouseEvent) =>
        {
            this.mouseX = event.offsetX - this.xc;
            this.mouseY = event.offsetY - this.yc;
        });

        // Touch support
        canvas.addEventListener("touchstart", (event: TouchEvent) =>
        {
            this.mouseDown = true;
            this.mouseX = event.touches[0].clientX - this.canvas.getBoundingClientRect().left - this.xc;
            this.mouseY = event.touches[0].clientY - this.canvas.getBoundingClientRect().top - this.yc;
        });
        canvas.addEventListener("touchend", () => { this.mouseDown = false; });
        canvas.addEventListener("touchmove", (event: TouchEvent) =>
        {
            this.mouseX = event.touches[0].clientX - this.canvas.getBoundingClientRect().left - this.xc;
            this.mouseY = event.touches[0].clientY - this.canvas.getBoundingClientRect().top - this.yc;
        });

        this.toggleButton = settings.getElementsByClassName("abort")[0] as HTMLButtonElement;
        this.toggleButton.onclick = () => this.toggle();

        this.context.font = "15px Arial";
        let text = this.context.measureText("1m");
        this.textWidth = text.width;

        // Set window size
        this.setWindowSizes();
    }

    /**
     * Toggles whether pendulum is running or not
     */
    public toggle()
    {
        if (this.running)
        {
            //clearInterval(this.interval);
            this.toggleButton.innerHTML = "Start!";
            this.running = false;
        }
        else
        {
            //this.lastTime = Date.now();
            //this.interval = setInterval(() => this.redraw(), 30);
            this.toggleButton.innerHTML = "Stop!";
            this.running = true;
        }
    }
    
    /**
     * Starts the pendulum drawing
     */
    public start()
    {
        this.lastTime = Date.now();
        this.running = true;
        this.interval = setInterval(() => this.redraw(), 30);
    }

    /**
     * Recalculate some values (like scale)
     */
    private onValueChange()
    {
        if (this.canvas.width > this.canvas.height)
        {
            this.scale = this.yc / (this.l1 + this.l2);
        }
        else
        {
            this.scale = this.xc / (this.l1 + this.l2);
        }

        this.scale *= 0.95;
    }

    /**
     * Resizes canvas to fit to window
     */
    private setWindowSizes()
    {
        let width = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        let height = window.innerHeight
            || document.body.clientHeight
            || document.documentElement.clientHeight;

        this.canvas.height = height * 0.9;
        if (width > height)
            this.canvas.width = width * 0.9 - settings.offsetWidth;
        else
            this.canvas.width = width * 0.9;

        this.xc = this.canvas.width / 2.0;
        this.yc = this.canvas.height / 2.0;

        this.onValueChange();
        this.redraw();
    }

    /**
     * Calculates one physical frame
     * @param dT Ellapsed time in Milliseconds
     */
    private calcPhysicalFrame(dT: number)
    {
        let denom = 2 * this.m1 + this.m2 - this.m2 * Math.cos(2 * this.a1 - 2 * this.a2);

        this.aa1 = (-this.g * (2 * this.m1 + this.m2) * Math.sin(this.a1) - this.m2 * this.g * Math.sin(this.a1 - 2 * this.a2) - 2 * Math.sin(this.a1 - this.a2) * this.m2 * (this.av2 * this.av2 * this.l2 + this.av1 * this.av1 * this.l1 * Math.cos(this.a1 - this.a2)))
            / (this.l1 * denom);

        this.aa2 = (2 * Math.sin(this.a1 - this.a2) * (this.av1 * this.av1 * this.l1 * (this.m1 + this.m2) + this.g * (this.m1 + this.m2) * Math.cos(this.a1) + this.av2 * this.av2 * this.l1 * this.m2 * Math.cos(this.a1 - this.a2)))
            / (this.l2 * denom);

        this.av1 += this.aa1 * dT / 1000;
        this.av2 += this.aa2 * dT / 1000;

        this.a1 += this.av1 * dT / 1000;
        this.a2 += this.av2 * dT / 1000;
    }

    /**
     * Does the drawing of one frame.
     */
    private redraw()
    {

        // Clear old stuff
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        if (this.mouseDown)
        {
            this.aa1 = 0.0;
            this.av1 = 0.0;
            this.aa2 = 0.0;
            this.av2 = 0.0;
            if (this.mouseX * this.mouseX + this.mouseY * this.mouseY < this.l1 * this.l1 * this.scale * this.scale)
            {
                if (Math.abs(this.mouseY) < 1)
                    this.a1 = Math.PI / 2.0 * Math.sign(this.mouseX);
                else if (this.mouseY < 0)
                    this.a1 = Math.atan(this.mouseX / this.mouseY) + Math.PI;
                else
                    this.a1 = Math.atan(this.mouseX / this.mouseY);
            }
            else
            {
                if (Math.abs(this.mouseY) < 1)
                    this.a1 = Math.PI / 2.0 * Math.sign(this.mouseX);
                else if (this.mouseY < 0)
                    this.a2 = Math.atan(this.mouseX / this.mouseY) + Math.PI;
                else
                    this.a2 = Math.atan(this.mouseX / this.mouseY);
            }
        }
        else if(this.running)
        {
            // calc elapsed time
            let dT = Date.now() - this.lastTime;

            // Physical frame calc
            while (dT > this.phyTime)
            {
                this.calcPhysicalFrame(this.timeScale * this.phyTime);
                dT -= this.phyTime;
            }

            // catch up to now
            if (dT > 0)
            {
                this.calcPhysicalFrame(this.timeScale * dT);
            }
        }

        // Calculate coordinates
        this.x1 = Math.floor(Math.sin(this.a1) * this.l1 * this.scale);
        this.y1 = Math.floor(Math.cos(this.a1) * this.l1 * this.scale);
        this.x2 = this.x1 + Math.floor(Math.sin(this.a2) * this.l2 * this.scale);
        this.y2 = this.y1 + Math.floor(Math.cos(this.a2) * this.l2 * this.scale);

        //add point:
        if(!(this.mouseDown || !this.running))
            this.tracePoints.push(new Point(this.x2 + this.xc, this.y2 + this.yc));
        while (this.tracePoints.length > this.numTracePoints)
            this.tracePoints.shift();

        //Draw path
        this.context.beginPath();
        this.context.lineWidth = 1;
        this.context.strokeStyle = "#FFFFFF";
        for (let p of this.tracePoints)
        {
            this.context.lineTo(p.x, p.y);
            this.context.moveTo(p.x, p.y);
        }
        this.context.stroke();

        //Draw scale
        this.context.beginPath();
        this.context.fillStyle = "#FFFFFF";
        this.context.strokeStyle = "#FFFFFF";
        this.context.lineWidth = 1;
        this.context.font = "15px Arial";
        this.context.fillText("1m",
            10 + (this.scale - this.textWidth + 10) / 2.0,
            this.canvas.height - 16);

        this.context.moveTo(20, this.canvas.height - 15);
        this.context.lineTo(20, this.canvas.height - 5);
        this.context.moveTo(20, this.canvas.height - 10);
        this.context.lineTo(10 + this.scale, this.canvas.height - 10);
        this.context.moveTo(10 + this.scale, this.canvas.height - 15);
        this.context.lineTo(10 + this.scale, this.canvas.height - 5);
        this.context.stroke();


        this.context.lineWidth = 2;
        //Second line
        this.context.beginPath();
        this.context.strokeStyle = "#0000FF";
        this.context.moveTo(this.xc + this.x1, this.yc + this.y1);
        this.context.lineTo(this.xc + this.x2, this.yc + this.y2);
        this.context.stroke();

        //Second mass
        this.context.beginPath();
        this.context.fillStyle = "#0000FF";
        this.context.arc(this.xc + this.x2, this.yc + this.y2, 5 * Math.sqrt(this.m2), 0.0, 2.0 * Math.PI);
        this.context.fill();

        //First line
        this.context.beginPath();
        this.context.strokeStyle = "#FF0000";
        this.context.moveTo(this.xc, this.yc);
        this.context.lineTo(this.xc + this.x1, this.yc + this.y1);
        this.context.stroke();

        //First mass
        this.context.beginPath();
        this.context.fillStyle = "#FF0000";
        this.context.arc(this.xc + this.x1, this.yc + this.y1, 5 * Math.sqrt(this.m1), 0.0, 2.0 * Math.PI);
        this.context.fill();

        this.energyText.innerHTML = (((this.m1 + this.m2) / 2.0) * this.l1 * this.l1 * this.av1 * this.av1 + (this.m2 / 2.0) * this.l2 * this.l2 * this.av2 * this.av2
            + this.m2 * this.l1 * this.l2 * this.av1 * this.av2 * Math.cos(this.a1 - this.a2) - (this.m1 + this.m2) * this.g * this.l1 * Math.cos(this.a1)
            - this.m2 * this.g * this.l2 * Math.cos(this.a2) + (this.m1 + this.m2) * this.g * this.l1
            + this.m2 * this.g * this.l2
        ).toFixed(3);

        // update time
        this.lastTime = Date.now();
    }
}

let canvas: HTMLCanvasElement = document.getElementById("myCanvas") as HTMLCanvasElement;
let settings = document.getElementById("settings") as HTMLDivElement;
let handler = new PendulumDrawer(canvas, settings);

handler.start();