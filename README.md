## [Live Demo](https://ludwigneste.gitlab.io/DoublePendulum/)


# Double Pendulum

A port of my [original C++ version](https://github.com/The-Ludwig/DoublePendulum) of the double pendulum.
More info on my original version's GitHub.